class DnsRecordsController < ApplicationController
  def index
    filter_dns_records = DnsRecordFilter.new(
      inclusions: index_params[:included], 
      exclusions: index_params[:excluded]
    ).call
    
    dns_records = filter_dns_records.result


    hostnames_difference = HostnamesDifference.new(
      inclusions: index_params[:included], 
      exclusions: index_params[:excluded]
    ).call
    
    hostnames = hostnames_difference.result

    render json: DnsRecordSerializer.new(dns_records: dns_records, hostnames: hostnames).parse
  end

  def create
    @dns_recod = DnsRecord.new(ip_address: dns_records_params[:ip])

    if @dns_recod.valid?
      @dns_recod.save!
      
      dns_records_params[:hostnames_attributes].to_a.each do |attribute|
        @dns_recod.hostnames << Hostname.create_or_find_by(hostname: attribute[:hostname])
      end
    end
  end

  
  private

  def dns_records_params
    params.require(:dns_records).permit(
      :ip, 
      hostnames_attributes: [
        :hostname
      ]
    )
  end

  def index_params
    {
      included: params[:included].to_s.split(','),
      excluded: params[:excluded].to_s.split(',')
    }
  end
end
