FROM ruby:2.6.6

RUN apt-get update -qq && apt-get install -y \
  build-essential \
  libpq-dev \
  liblzma-dev \  
  wget \ 
  unzip \
  libgconf2-4

WORKDIR /code
COPY Gemfile /code/Gemfile
COPY Gemfile.lock /code/Gemfile.lock

RUN gem install bundler --no-document -v $BUNDLER_VERSION

RUN bundler install

COPY . /code