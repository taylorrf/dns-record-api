class DnsRecordSerializer
  attr_reader :dns_records, :hostnames

  def initialize(dns_records:, hostnames:)
    @dns_records = dns_records
    @hostnames = hostnames
  end

  def parse
    {
      total_records: total_records,
      records: records,
      related_hostnames: related_hostnames
    }
  end


  private

  def total_records
    dns_records.size
  end

  def records
    dns_records.map do |dns_record|
      { id: dns_record.id, ip_address: dns_record.ip_address }
    end
  end

  def related_hostnames
    hostnames.map do |hostname|
      { hostname: hostname.first, count: hostname.second.size }
    end
  end
end