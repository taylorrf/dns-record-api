class DnsRecord < ApplicationRecord
  validates :ip_address, presence: true

  has_many :dns_record_hostnames
  has_many :hostnames, through: :dns_record_hostnames
end
