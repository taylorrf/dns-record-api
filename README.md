# DNS RECORDS API

It's an API to create DNS records and their associated hostnames.

The API have two main endpoints:

### Create DNS records

A DNS record may have many hostnames.

Example

````
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"dns_records": { "ip":"6.6.6.6", "hostnames_attributes": [{"hostname": "lorem.com"}, {"hostname": "ipsum.com"}] } }' \
  http://localhost:3000/dns_records
````

### Query by DNS records

Example

````
curl -X GET --data "included=lorem.com,ipsum.com&excluded=sit.com" http://localhost:3000/dns_records
````

## Instructions:

* Ruby version
2.2.6

**Pre-requisites**  

* [Docker](https://www.docker.com/)

* [Docker-compose](https://docs.docker.com/compose/install/)

**Installation**
Create the image<br>
`$ docker-compose build`

* Database initialization<br>
`$ docker-compose run web rake db:create`
`$ docker-compose run web rake db:migrate`
`$ docker-compose run web rake db:seed`

Run Rails application<br>
`$ docker-compose up`

you can access application at http://localhost:3000

**How to run the test**

Execute bash inside of a container<br>
`$ docker-compose run --rm -e RAILS_ENV=test web bash`
`$ rspec`



