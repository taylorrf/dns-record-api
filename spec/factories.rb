FactoryBot.define do
  factory :dns_record do
    ip_address { "1.1.1.1" } 
  end

  factory :hostname do
    hostname { "host.com" }
  end

  factory :dsn_record_hostname do
    dns_record
    hostname
  end
end