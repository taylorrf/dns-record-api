require 'rails_helper'

RSpec.describe "DNS Records Response", type: :request do
  context 'when endpoint was queried' do
    let(:host_lorem) { create(:hostname, hostname: 'lorem.com') }
    let(:host_ipsum) { create(:hostname, hostname: 'ipsum.com') }
    let(:host_dolor) { create(:hostname, hostname: 'dolor.com') }
    let(:host_amet) { create(:hostname, hostname: 'amet.com') }
    let(:host_sit) { create(:hostname, hostname: 'sit.com') }

    setup do
      dsn1 = create(:dns_record, id: 1, ip_address: '1.1.1.1')
      dsn1.hostnames << host_lorem
      dsn1.hostnames << host_ipsum
      dsn1.hostnames << host_dolor
      dsn1.hostnames << host_amet

      dsn2 = create(:dns_record, id: 2, ip_address: '2.2.2.2')
      dsn2.hostnames << host_ipsum

      dsn3 = create(:dns_record, id: 3, ip_address: '3.3.3.3')
      dsn3.hostnames << host_ipsum
      dsn3.hostnames << host_dolor
      dsn3.hostnames << host_sit
      dsn3.hostnames << host_amet
    end

    describe 'the `total_records` attribute' do
      it 'includes impsum host' do
        get '/dns_records', params: { included: host_ipsum.hostname }

        response_body = JSON.parse(response.body)

        expect(response_body['total_records']).to eq(3)
      end

      it 'includes impsum host but exclude sit host' do
        get '/dns_records', params: { included: host_ipsum.hostname, excluded: host_sit.hostname }

        response_body = JSON.parse(response.body)

        expect(response_body['total_records']).to eq(2)
      end

      it 'includes impsum host but exclude dolor host' do
        get '/dns_records', params: { included: host_ipsum.hostname, excluded: host_dolor.hostname }

        response_body = JSON.parse(response.body)

        expect(response_body['total_records']).to eq(1)
      end

      it 'includes some unknow host' do
        get '/dns_records', params: { included: 'what.com' }

        response_body = JSON.parse(response.body)

        expect(response_body['total_records']).to eq(0)
      end

      it 'dont provides query params' do
        get '/dns_records'

        response_body = JSON.parse(response.body)

        expect(response_body['total_records']).to eq(0)
      end
    end

    describe 'the `records` list' do
      it 'includes impsum host' do
        get '/dns_records', params: { included: host_ipsum.hostname }

        response_body = JSON.parse(response.body)
        records = response_body['records']

        expect(records).to include({"id" => 1, "ip_address"=> "1.1.1.1"})
        expect(records).to include({"id" => 2, "ip_address"=> "2.2.2.2"})
        expect(records).to include({"id" => 3, "ip_address"=> "3.3.3.3"})
      end

      it 'includes impsum host but exclude sit host' do
        get '/dns_records', params: { included: host_ipsum.hostname, excluded: host_sit.hostname }

        response_body = JSON.parse(response.body)
        records = response_body['records']

        expect(records).to include({"id" => 1, "ip_address"=> "1.1.1.1"})
        expect(records).to include({"id" => 2, "ip_address"=> "2.2.2.2"})
        expect(records).to_not include({"id" => 3, "ip_address"=> "3.3.3.3"})
      end

      it 'includes impsum host but exclude dolor host' do
        get '/dns_records', params: { included: host_ipsum.hostname, excluded: host_dolor.hostname }

        response_body = JSON.parse(response.body)
        records = response_body['records']

        expect(records).to_not include({"id" => 1, "ip_address"=> "1.1.1.1"})
        expect(records).to include({"id" => 2, "ip_address"=> "2.2.2.2"})
        expect(records).to_not include({"id" => 3, "ip_address"=> "3.3.3.3"})
      end

      it 'includes some unknow host' do
        get '/dns_records', params: { included: 'what.com' }

        response_body = JSON.parse(response.body)
        records = response_body['records']

        expect(records).to match_array([])

        expect(records).to_not include({"id" => 1, "ip_address"=> "1.1.1.1"})
        expect(records).to_not include({"id" => 2, "ip_address"=> "2.2.2.2"})
        expect(records).to_not include({"id" => 3, "ip_address"=> "3.3.3.3"})
      end

      it 'do not provides query params' do
        get '/dns_records'

        response_body = JSON.parse(response.body)
        records = response_body['records']

        expect(records).to match_array([])
        expect(records).to_not include({"id" => 1, "ip_address"=> "1.1.1.1"})
        expect(records).to_not include({"id" => 2, "ip_address"=> "2.2.2.2"})
        expect(records).to_not include({"id" => 3, "ip_address"=> "3.3.3.3"})
      end
    end

    describe 'the `related_hostnames` list' do
      it 'gives the difference between hostnames specified on query params' do
        get '/dns_records', params: { included: "ipsum.com,dolor.com", excluded: 'sit.com' }

        response_body = JSON.parse(response.body)
        hostnames = response_body['related_hostnames']

        expect(hostnames).to include({"hostname"=>"lorem.com", "count"=>1})
        expect(hostnames).to include({"hostname"=>"amet.com", "count"=>2})
      end

      it 'do not provides query params' do
        get '/dns_records'

        response_body = JSON.parse(response.body)
        hostnames = response_body['related_hostnames']

        expect(hostnames).to match_array([])
      end
    end
  end
end
