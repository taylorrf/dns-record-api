class DnsRecordFilter
  prepend SimpleCommand

  def initialize(inclusions:, exclusions:)
    @inclusions = inclusions
    @exclusions = exclusions
  end

  def call
    (inclusions_set - exclusions_set).to_a
  end

  private

  def inclusions_set
    return [].to_set unless @inclusions.present?

    DnsRecord.includes(:hostnames).where(hostnames: {hostname: @inclusions}).to_set
  end

  def exclusions_set
    return [].to_set unless @exclusions.present?

    DnsRecord.includes(:hostnames).where(hostnames: {hostname: @exclusions}).to_set
  end
end