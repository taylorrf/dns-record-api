require 'rails_helper'

RSpec.describe "Dns Records Creation", type: :request do
  let(:headers) { { "CONTENT_TYPE" => "application/json" } }

  describe 'how to create a new dns record' do
    it 'requires an dns ip attribute' do
      expect(DnsRecord.where(ip_address: '6.6.6.6').count).to be_zero

      request_body = '{ "dns_records": { "ip":"6.6.6.6" } }'

      expect {
        post '/dns_records', params: request_body, headers: headers
      }.to change { DnsRecord.count }.by(1)
    end

    it 'can provide hostnames related to the new dns record' do
      request_body = '{"dns_records": { "ip":"6.6.6.6", "hostnames_attributes": [{"hostname": "lorem.com"}, {"hostname": "ipsum.com"}] } }'
      post '/dns_records', params: request_body, headers: headers

      dns_record = DnsRecord.where(ip_address: '6.6.6.6')

      expect(dns_record.count).to eq(1)
      expect(dns_record.first.hostnames.count).to eq(2)
    end
  end
end