class HostnamesDifference
  prepend SimpleCommand
  attr_reader :inclusions, :exclusions, :dns_records

  def initialize(inclusions:, exclusions:)
    @inclusions = inclusions
    @exclusions = exclusions
  end

  def call
    dns_records_hostnames.flatten.group_by(&:hostname).to_a
  end

  private

  def dns_records_hostnames
    dns_records.map do |dns_record|
      dns_record.hostnames.where.not(hostname: query_parameters)
    end
  end

  def dns_records
    dns_records ||= DnsRecord.includes(:hostnames).where(hostnames: {hostname: inclusions})
  end

  def query_parameters
    (inclusions + exclusions).uniq
  end
end