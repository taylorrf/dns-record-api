require 'rails_helper'

RSpec.describe DnsRecord, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:ip_address) }
  end

  describe 'associations' do
    it { should have_many(:dns_record_hostnames) }
    it { should have_many(:hostnames).through(:dns_record_hostnames) }
  end
end
