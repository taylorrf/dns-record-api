Rails.application.config.generators do |g|
  g.test_framework :rspec, spec: true, fixture: false
  g.fixture_replacement :factory_bot
end