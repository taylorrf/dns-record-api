require 'rails_helper'

RSpec.describe DnsRecordHostname, type: :model do
  describe 'associations' do
    it { should belong_to(:hostname) }
    it { should belong_to(:dns_record) }
  end
end
